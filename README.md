# email-templates

Prerequisites:
```
npm install
```

Develop templates:
```
npm run watch
```

Build all templates:
```
npm run build
```
